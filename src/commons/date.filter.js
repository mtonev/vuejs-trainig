export default date => {
  return new Date(date).toUTCString();
};