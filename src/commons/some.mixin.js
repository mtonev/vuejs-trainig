export default {
    data: function () {
        return {
            mixintext: 'Initial text'
        }
    },
    created: function () {
        this.hello()
    },
    mounted: function () {
        this.mixintext = 'some text';
    },
    methods: {
        hello: function () {
            console.log('hello() called: ', this.mixintext)
        }
    }
}
