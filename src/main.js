import Vue from 'vue'
import App from './App.vue'
import router from './router'
import DateFilter from './commons/date.filter'
import CapitalizeFilter from './commons/capitalize.filter'
// import HelloWorld from "./components/HelloWorld";
// Vue.component('hello-world', HelloWorld)

Vue.filter("date", DateFilter);
Vue.filter("capitalize", CapitalizeFilter);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
