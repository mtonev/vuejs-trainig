import Vue from "vue";
import Router from "vue-router";
import Home from "./Home.vue";

Vue.use(Router);

export default new Router({
    mode: "history",
    base: '/',
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/todo-demo",
            name: "todo-demo",
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import(/* webpackChunkName: "ToDoDemo" */ "./components/ToDoDemo")
        },
        {
            path: "*",
            name: "404",
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import(/* webpackChunkName: "404" */ "./components/404")
        }
    ]
});
